import java.util.Random;

public class RouletteWheel {
    private Random rng = new Random();
    private int number = 0;

    public RouletteWheel() {

    }

    public void spin() {
        this.number = rng.nextInt(38);
    }

    public int getValue() {
        return this.number;
    }

    public int winOrLose(int pBetNum, int pBet, char colour, char numType) {
        int burntOrWon = 0;
        if(this.number == pBetNum) {
            burntOrWon += pBet*35;
        } else {
            burntOrWon += -1*pBet;
        }

        if(isRed() && colour == 'R') {
            burntOrWon += pBet;
        }

        if(isBlack() && colour == 'B') {
            burntOrWon += pBet;
        }

        if(isOdd() && numType == 'O') {
            burntOrWon += pBet;
        }

        if(isEven() && numType == 'E') {
            burntOrWon += pBet;
        }

        return burntOrWon;
    }

    public boolean isLow() {
        if(this.number <= 18) {
            return true;
        }
        return false;
    }

    public boolean isHigh() {
        if(this.number >= 19) {
            return true;
        }
        return false;
    }

    public boolean isEven() {
        if(this.number % 2 == 0 && this.number != 0) {
            return true;
        }
        return false;
    }

    public boolean isOdd() {
        if(this.number % 2 > 0 && this.number != 0) {
            return true;
        }
        return false;
    }

    public boolean isRed() {
        if(((this.number > 0 && this.number < 11 && this.number % 2 > 0) || (this.number > 10 && this.number < 19 && this.number % 2 == 0) || 
            (this.number > 18 && this.number < 28 && this.number % 2 > 0) || (this.number > 28 && this.number < 37 && this.number % 2 == 0)) && this.number != 0) {
            return true;
        }
        return false;
    }

    public boolean isBlack() {
        if(!(isRed()) && this.number != 0) {
            return true;
        }
        return false;
    }
}
