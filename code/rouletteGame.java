import java.util.Scanner;

public class rouletteGame {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        RouletteWheel wheel = new RouletteWheel();

        int playerMon = 1000;
        int moneyBurntOrWon = 0;
        boolean cont = true;
        
        System.out.println("Would you like to burn your money away? (Y/N)\n");
        char confirm = sc.next().toUpperCase().charAt(0);

        while(confirm != 'Y' && confirm != 'N') {
            try {
                throw new IllegalArgumentException("Invalid confirmation. Please try again:");
            }
            catch(IllegalArgumentException invalidConf) {
                System.out.println(invalidConf.getMessage());
            }
            confirm = sc.next().toUpperCase().charAt(0);
        }

        if(confirm == 'N') {
            cont = false;
        }
        
        while(playerMon > 0 && cont) {
            System.out.println("Which number in the roulette do you bet for?");
            int betNum = sc.nextInt();
            while(betNum < 0 || betNum > 36) {
                try {
                    throw new IllegalArgumentException("Invalid value! Try again:");
                }
                catch(IllegalArgumentException invalidBet) {
                    System.out.println(invalidBet.getMessage());
                }

                betNum = sc.nextInt();
            }

            System.out.println("Which colour?\nR:Red\nB:Black");
            char colour = sc.next().toUpperCase().charAt(0);
            while(colour != 'R' && colour != 'B') {
                try {
                    throw new IllegalArgumentException("Invalid colour! Try again:");
                }
                catch(IllegalArgumentException invalidColour) {
                    System.out.println(invalidColour.getMessage());
                }
                colour = sc.next().toUpperCase().charAt(0);
            }

            System.out.println("Odd or Even?\nO:Odd\nE:Even");
            char numType = sc.next().toUpperCase().charAt(0);
            while(numType != 'O' && numType != 'E') {
                try {
                    throw new IllegalArgumentException("Invalid colour! Try again:");
                }
                catch(IllegalArgumentException invalidType) {
                    System.out.println(invalidType.getMessage());
                }
                numType = sc.next().toUpperCase().charAt(0);
            }

            System.out.println("How much will you bet?");
            int bet = sc.nextInt();
            while(bet > playerMon) {
                System.out.println("Invalid bet. Please try again:");
                bet = sc.nextInt();
            }
            
            /*wheel.spin();*/
            moneyBurntOrWon += wheel.winOrLose(betNum, bet, colour, numType);
            playerMon += wheel.winOrLose(betNum, bet, colour, numType);
            
            System.out.println("Your Current Money: " + playerMon + "$");

            if(playerMon != 0) {
                System.out.println("End game? (Y/N)");
                confirm = sc.next().toUpperCase().charAt(0);

                while(confirm != 'Y' && confirm != 'N') {
                    try {
                        throw new IllegalArgumentException("Invalid confirmation. Please try again:");
                    }
                    catch(IllegalArgumentException invalidConf) {
                        System.out.println(invalidConf.getMessage());
                    }
                    confirm = sc.next().toUpperCase().charAt(0);
                }

                if(confirm == 'Y') {
                    cont = false;
                }
            } else {
                System.out.println("Congratulations, you're officially broke and you can no longer pay for your home's unpaid mortgage, just like me!");
            }
        }

        System.out.println("Your Final Money Count: " + playerMon + "$");
        if(moneyBurntOrWon > 0) {
            System.out.println("Money Won:" + moneyBurntOrWon);
        } else {
            System.out.println("Money Burnt: " + moneyBurntOrWon*-1);
        }
    }
}